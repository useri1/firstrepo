package Example;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FileOps {

    public static final String filename = "blogs.csv";

    public static void saveToFile(List<String> stringList) throws IOException {
        FileWriter writer = new FileWriter(filename);

        String collect = stringList.stream().collect(Collectors.joining("\n"));
        writer.write(collect);
        writer.close();
    }

    public static ArrayList<String> getListFromFile() throws FileNotFoundException {

        Scanner s = new Scanner(new File(filename));
        ArrayList<String> list = new ArrayList<>();
        while (s.hasNext()) {
            list.add(s.next());
        }
        s.close();
        return list;
    }

}
