import Example.FileOps;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class BlogView extends JApplet implements ListSelectionListener {
    ArrayList<String> listBlogs = new ArrayList<String>();

    JList<String> listFromFile;
    DefaultListModel<String> listModel;
    JTextArea textArea;
    boolean firstClick = false;

    public static void main(String[] args) {
        BlogView blogView = new BlogView();
        blogView.getListFromFile();
        blogView.showUI();
    }

    public void getListFromFile() {
        try {
            ArrayList<String> listString = FileOps.getListFromFile();
            listModel = new DefaultListModel<>();

            for(int i = 0; i < listString.size(); i++) {
                listModel.addElement(listString.get(i));
            }
            listFromFile = new JList(listModel);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void showUI() {
        JFrame frame = new JFrame();
        JPanel higerPanel = new JPanel();
        JPanel lowerPanel = new JPanel();

        higerPanel.setBackground(Color.WHITE);
        lowerPanel.setBackground(Color.GRAY);

        higerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setResizeWeight(0.7);
        splitPane.setEnabled(true);
        splitPane.setDividerSize(0);

        //set layout manager Y_AXIS - vertilcal from the left to right X_Axis - horisontal
//        higerPanel.setLayout(new BoxLayout(higerPanel, BoxLayout.PAGE_AXIS));
//        lowerPanel.setLayout(new BoxLayout(lowerPanel, BoxLayout.X_AXIS));
        higerPanel.setLayout(new BorderLayout(5, 5));
        lowerPanel.setLayout(new GridLayout(1, 2, 5, 0));
        higerPanel.add(lowerPanel, BorderLayout.SOUTH);

        //add scrollers to higerPanel
        higerPanel.add(BorderLayout.NORTH, createTextScroller());
        higerPanel.add(BorderLayout.CENTER, creatEditScroller());

        //edit list options
        listFromFile.setVisibleRowCount(10);
        listFromFile.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listFromFile.addListSelectionListener(this);

        //add 3 buttons
        lowerPanel.add(getIndexButton());
        lowerPanel.add(saveListButton());
        lowerPanel.add(putToListButton());

        frame.getContentPane().add(BorderLayout.CENTER, higerPanel);
        frame.getContentPane().add(BorderLayout.SOUTH, lowerPanel);
        frame.setSize(1000, 500);
        frame.setVisible(true);
    }

    //save current list to csv file
    public JButton saveListButton() {
        JButton saveButton = new JButton("Save all values");
        saveButton.addActionListener(actionEvent -> {
            try {
                FileOps.saveToFile(listBlogs);
                System.out.println("Saved to file");
                getListFromFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return saveButton;
    }

    //get choosed value of index
    public JButton getIndexButton() {
        JButton listButton = new JButton("Choosed value");
        listButton.addActionListener(actionEvent ->  {
            System.out.println("Choosed value: " + listFromFile.getSelectedValue());
        });
        return listButton;
    }

    public JButton putToListButton() {
        JButton saveButton = new JButton("Put to list");
        saveButton.addActionListener(actionEvent -> {
            if (textArea.getText() != null) {
                String newElement = textArea.getText();
                listBlogs.add(newElement);
                listModel.addElement(newElement);

                listFromFile.ensureIndexIsVisible(listModel.getSize() + 1);
                listBlogs.add(textArea.getText());
                try {
                    FileOps.saveToFile(listBlogs);
                    getListFromFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("List contains: " + Arrays.toString(listBlogs.toArray()));
            }
        });
        return saveButton;
    }

//default method of showing pointed index
    public void valueChanged(ListSelectionEvent lse) {
        if (!lse.getValueIsAdjusting()) {
            int selection = listFromFile.getSelectedIndex();
            int listElementNumber = selection + 1;
            System.out.println("Selected index: " + listElementNumber);
        }
    }
//bottom scroller
    public JScrollPane creatEditScroller() {
        textArea = new JTextArea(10, 33);
        textArea.setLineWrap(true); //turn on text wrap
        textArea.setText("Set text");

        JScrollPane scroller = new JScrollPane(textArea); //set text area that has to be scrolled
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); //only vertical scroll
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);//only vertical scroll
        //checking for first click
        textArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (firstClick==false) {
                    firstClick = true;
                    textArea.setText(null);
                }
            }
        });
        return scroller;
    }

    public JScrollPane createTextScroller() {
        JScrollPane listPane = new JScrollPane(listFromFile);
        listPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        listPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        return listPane;
    }
}
