package Interfaces;

import java.util.List;

public interface BlogViewInterface {

    void getCurrentInfo();
    void displayBlogInfo(List<BlogData> blogData);
    void setListener();
}
