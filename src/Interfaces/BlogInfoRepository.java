package Interfaces;

import java.util.List;

public interface BlogInfoRepository {

    void saveBlogList(List<BlogData> blogDataList);

    List<BlogData> getBlogList();
}
